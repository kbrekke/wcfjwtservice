﻿using System.ServiceModel;

namespace WcfJwtService
{
    [ServiceContract]
    public interface IClaimsService
    {
        [OperationContract]
        string GetClaims();
    }
}