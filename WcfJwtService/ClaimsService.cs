﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IdentityModel.Configuration;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

namespace WcfJwtService
{
    public class ClaimsService : IClaimsService
    {
        public static void Configure(ServiceConfiguration config)
        {
            config.IdentityConfiguration = CreateIdentityConfiguration();
            config.UseIdentityConfiguration = true;
            config.Description.Behaviors.Add(new ServiceAuthorizationBehavior());

            var authz = config.Description.Behaviors.Find<ServiceAuthorizationBehavior>();
            authz.PrincipalPermissionMode = PrincipalPermissionMode.Always;

            config.AddServiceEndpoint(typeof(IClaimsService), CreateBinding(), "https://localhost:44305/token");
        }

        private static IdentityConfiguration CreateIdentityConfiguration()
        {
            var identityConfiguration = new IdentityConfiguration();

            identityConfiguration.SecurityTokenHandlers.Clear();
            identityConfiguration.SecurityTokenHandlers.Add(new IdentityServerWrappedJwtHandler("https://localhost:44300/identity", "wcfJwtService"));
            identityConfiguration.ClaimsAuthorizationManager = new RequireAuthenticationAuthorizationManager();

            return identityConfiguration;
        }

        private static Binding CreateBinding()
        {
            var binding = new WS2007FederationHttpBinding(WSFederationHttpSecurityMode.TransportWithMessageCredential);

            // only for testing on localhost
            binding.HostNameComparisonMode = HostNameComparisonMode.Exact;

            binding.Security.Message.EstablishSecurityContext = false;
            binding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;

            return binding;
        }

        public string GetClaims()
        {
            var claimsPrincipal = OperationContext.Current.ClaimsPrincipal;
            var identity = claimsPrincipal.Identity as ClaimsIdentity;

            var claims = new List<SimpleClaim>();

            foreach (var c in identity.Claims)
            {
                claims.Add(new SimpleClaim
                {
                    Type = c.Type,
                    Value = c.Value
                });
            }

            return JsonConvert.SerializeObject(claims);
        }

        private class SimpleClaim
        {
            public string Type { get; set; }
            public string Value { get; set; }
        }
    }
}