﻿using IdentityModel.Client;
using IdentityModel.Constants;
using IdentityModel.Extensions;
using System;
using System.IdentityModel.Tokens;
using System.Security.Claims;
using System.ServiceModel;
using System.Xml.Linq;
using WcfJwtService;

namespace WcfJwtClient
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var jwt = GetJwt();
            var xmlToken = WrapJwt(jwt);

            var binding = new WS2007FederationHttpBinding(WSFederationHttpSecurityMode.TransportWithMessageCredential);
            binding.HostNameComparisonMode = HostNameComparisonMode.Exact;
            binding.Security.Message.EstablishSecurityContext = false;
            binding.Security.Message.IssuedKeyType = SecurityKeyType.BearerKey;

            var factory = new ChannelFactory<IClaimsService>(
                binding,
                new EndpointAddress("https://localhost:44305/token"));

            var channel = factory.CreateChannelWithIssuedToken(xmlToken);

            Console.WriteLine(channel.GetClaims());
            Console.ReadKey();
        }

        private static GenericXmlSecurityToken WrapJwt(string jwt)
        {
            var subject = new ClaimsIdentity("saml");
            subject.AddClaim(new Claim("jwt", jwt));

            var descriptor = new SecurityTokenDescriptor
            {
                TokenType = TokenTypes.Saml2TokenProfile11,
                TokenIssuerName = "urn:wrappedjwt",
                Subject = subject
            };

            var handler = new Saml2SecurityTokenHandler();
            var token = handler.CreateToken(descriptor);

            var xmlToken = new GenericXmlSecurityToken(
                XElement.Parse(token.ToTokenXmlString()).ToXmlElement(),
                null,
                DateTime.Now,
                DateTime.Now.AddHours(1),
                null,
                null,
                null);

            return xmlToken;
        }

        private static string GetJwt()
        {
            var oauth2Client = new TokenClient(
                Constants.TokenEndpoint,
                "wcfClient",
                "wcfSecret");

            var tokenResponse =
                oauth2Client.RequestResourceOwnerPasswordAsync("nhn", "pass", "wcfJwtService").Result;

            return tokenResponse.AccessToken;
        }
    }
}